# 实验1：用户及权限管理

- 学号：202010414204，姓名：谷文强，班级：2班

## 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验内容

（1）对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
（2）设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，
通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 实验步骤

- 步骤1
  - 分配权限
    ![img1.png](./img1.png)


- 步骤2
  - 用两种方式查询两个部门('IT'和'Sales')的部门总人数和平均工资
  - （1）查询1
  - ![img2.png](./img2.png)
  - （2）查询2
  - ![img3.png](./img3.png)

## 结论

