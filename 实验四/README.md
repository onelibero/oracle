# 实验4：PL/SQL语言打印杨辉三角
## 班级：软件工程2020级2班
## 学号：202010414204
## 姓名：谷文强
## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## 杨辉三角
- 将源代码转为存储过程：
```sql
CREATE OR REPLACE PROCEDURE YHTriangle (N IN INTEGER) IS
    TYPE t_number IS VARRAY (100) OF INTEGER NOT NULL;
    i INTEGER;
    j INTEGER;
    spaces VARCHAR2(30) :='   ';
    rowArray t_number := t_number();
BEGIN
    DBMS_OUTPUT.put_line('1');
    DBMS_OUTPUT.put(RPAD(1,N*9,' '));
    DBMS_OUTPUT.put_line('');
    rowArray.extend(2);
    rowArray(1):=1;
    rowArray(2):=1;
    FOR i IN 3 .. N
    LOOP
        rowArray.extend(i);
        rowArray(i):=1;
        j:=i-1;
        WHILE j>1
        LOOP
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        END LOOP;
        FOR j IN 1 .. i
        LOOP
            DBMS_OUTPUT.put(RPAD(rowArray(j),9,' '));
        END LOOP;
        DBMS_OUTPUT.put_line('');
    END LOOP;
END;
/

```
![](img1.png)
- 调用这个过程打印杨辉三角：
```
BEGIN
    YHTriangle(N => 10); --将 10 替换为想要打印的行数
END;
/

```
![](img2.png)
## 实验注意事项

- 完成时间：2023-05-15，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test4目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
