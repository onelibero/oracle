<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)
## 班级：软件工程2020级2班
## 学号：202010414204
## 姓名：谷文强
- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。
## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 总体方案

#### 表空间设计方案：

- 创建两个表空间：sales_data和sales_index.
- sales_data用于存储数据表，包括商品、订单、客户等信息。
- sales_index用于存储索引，提高查询性能。
#### 表设计方案：
- a. 商品表（products）：


- 列：product_id（主键），product_name，price，quantity，category_id等。
- b. 订单表（orders）：
- 列：order_id（主键），customer_id（外键），order_date，total_amount等。
- c. 客户表（customers）：
- 列：customer_id（主键），customer_name，address，phone等。
- d. 分类表（categories）：
- 列：category_id（主键），category_name等。
#### 用户和权限分配方案：

- 创建用户：sales_user和sales_admin。
- sales_user用户用于应用程序连接，具有读取和写入表的权限。
- sales_admin用户用于管理员连接，具有所有权限，包括表的创建和修改。
#### 程序包和存储过程设计方案：

- 创建程序包：sales_pkg。
- 在程序包中设计存储过程和函数，实现复杂的业务逻辑，例如：
- create_order：创建订单，更新商品数量和订单金额。
- get_customer_orders：获取特定客户的所有订单信息。
- calculate_total_sales：计算总销售额。
- get_top_selling_products：获取畅销商品列表等。
#### 备份方案：

- 定期进行完整数据库备份，包括数据文件和控制文件。
- 使用Oracle的备份工具，如RMAN（Recovery Manager）进行备份。
- 将备份文件保存在不同的位置，以防止单点故障。
- 定期测试和还原备份，以确保备份的可用性和完整性。

### 具体步骤

##### 第1步：表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。

```sql
-- 创建主表空间
CREATE TABLESPACE sales_data
DATAFILE '/home/oracle/Desktop/sales_data.dbf' SIZE 1G
AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED;

-- 创建索引表空间
CREATE TABLESPACE sales_data
DATAFILE '/home/oracle/Desktop/sales_index.dbf' SIZE 500M
AUTOEXTEND ON NEXT 50M MAXSIZE UNLIMITED;
  ```
 ![](img1.png)

```sql
-- 创建分类表
CREATE TABLE categories (
  category_id NUMBER PRIMARY KEY,
  category_name VARCHAR2(100)
);

-- 创建商品表
CREATE TABLE products (
  product_id NUMBER PRIMARY KEY,
  product_name VARCHAR2(100),
  price NUMBER,
  quantity NUMBER,
  category_id NUMBER,
  CONSTRAINT fk_category_id FOREIGN KEY (category_id)
    REFERENCES categories(category_id)
);

-- 创建客户表
CREATE TABLE customers (
  customer_id NUMBER PRIMARY KEY,
  customer_name VARCHAR2(100),
  address VARCHAR2(100),
  phone VARCHAR2(20)
);

-- 创建订单表
CREATE TABLE orders (
  order_id NUMBER PRIMARY KEY,
  customer_id NUMBER,
  order_date DATE,
  total_amount NUMBER,
  CONSTRAINT fk_customer_id FOREIGN KEY (customer_id)
    REFERENCES customers(customer_id)
);
```
![](img2.png)

```sql
-- 插入模拟数据
-- 在 categories 表中插入模拟数据
BEGIN
  FOR i IN 1..5 LOOP
    INSERT INTO categories (category_id, category_name)
    VALUES (i, 'Category ' || i);
  END LOOP;

  COMMIT;
END;
/
-- 在 products 表中插入十万条模拟数据
DECLARE
  v_product_id NUMBER;
  v_product_name VARCHAR2(50);
  v_price NUMBER;
  v_quantity NUMBER;
  v_category_id NUMBER;
BEGIN
  FOR i IN 1..100000 LOOP
    -- 生成随机数据
    v_product_id := i;
    v_product_name := 'Product ' || i;
    v_price := ROUND(DBMS_RANDOM.VALUE(10, 100), 2); -- 生成10到100之间的随机价格
    v_quantity := ROUND(DBMS_RANDOM.VALUE(1, 100), 0); -- 生成1到100之间的随机数量
    v_category_id := ROUND(DBMS_RANDOM.VALUE(1, 5), 0); -- 假设有5个分类，生成1到5之间的随机分类ID

    -- 插入数据到 products 表
    INSERT INTO products (product_id, product_name, price, quantity, category_id)
    VALUES (v_product_id, v_product_name, v_price, v_quantity, v_category_id);

    -- 每插入1000条数据提交一次事务
    IF MOD(i, 1000) = 0 THEN
      COMMIT;
    END IF;
  END LOOP;

  -- 提交剩余的数据
  COMMIT;
END;
/
-- 在 customers 表中插入十万条模拟数据
DECLARE
  v_customer_id NUMBER;
  v_customer_name VARCHAR2(50);
  v_address VARCHAR2(100);
  v_phone VARCHAR2(20);
BEGIN
  FOR i IN 1..100000 LOOP
    -- 生成随机数据
    v_customer_id := i;
    v_customer_name := 'Customer ' || i;
    v_address := 'Address ' || i;
    v_phone := 'Phone ' || i;

    -- 插入数据到 customers 表
    INSERT INTO customers (customer_id, customer_name, address, phone)
    VALUES (v_customer_id, v_customer_name, v_address, v_phone);

    -- 每插入1000条数据提交一次事务
    IF MOD(i, 1000) = 0 THEN
      COMMIT;
    END IF;
  END LOOP;

  -- 提交剩余的数据
  COMMIT;
END;
/

```
![](img3.png)
![](img4.png)

##### 第2步：设计权限及用户分配方案。至少两个用户。
```sql
-- 创建SALE_USER用户并分配权限
CREATE USER sales_user IDENTIFIED BY password;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sales_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO sales_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO sales_user;
GRANT SELECT ON categories TO sales_user;


-- 创建SALE_ADMIN用户并分配权限
CREATE USER sales_admin IDENTIFIED BY password;
GRANT ALL PRIVILEGES TO sales_admin;


```
![](img5.png)

##### 第3步：在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
```sql
-- 创建程序包 sales_pkg
CREATE OR REPLACE PACKAGE sales_pkg AS

  -- 创建订单，更新商品数量和订单金额
  PROCEDURE create_order(p_customer_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER);

  -- 获取特定客户的所有订单信息
  FUNCTION get_customer_orders(p_customer_id IN NUMBER) RETURN SYS_REFCURSOR;

  -- 计算总销售额
  FUNCTION calculate_total_sales RETURN NUMBER;

  -- 获取畅销商品列表
  FUNCTION get_top_selling_products(p_limit IN NUMBER) RETURN SYS_REFCURSOR;

END sales_pkg;
/

-- 实现程序包 sales_pkg
CREATE OR REPLACE PACKAGE BODY sales_pkg AS

  -- 创建订单，更新商品数量和订单金额
  PROCEDURE create_order(p_customer_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER) IS
    v_product_price NUMBER;
    v_order_id NUMBER;
    v_total_amount NUMBER;
  BEGIN
    -- 获取商品价格
    SELECT price INTO v_product_price FROM products WHERE product_id = p_product_id;

    -- 插入订单数据
    INSERT INTO orders (order_id, customer_id, order_date)
    VALUES (orders_seq.NEXTVAL, p_customer_id, SYSDATE)
    RETURNING order_id INTO v_order_id;

    -- 更新商品数量
    UPDATE products SET quantity = quantity - p_quantity WHERE product_id = p_product_id;

    -- 计算订单金额
    v_total_amount := v_product_price * p_quantity;

    -- 更新订单金额
    UPDATE orders SET total_amount = v_total_amount WHERE order_id = v_order_id;
  END create_order;

  -- 获取特定客户的所有订单信息
  FUNCTION get_customer_orders(p_customer_id IN NUMBER) RETURN SYS_REFCURSOR IS
    v_result SYS_REFCURSOR;
  BEGIN
    OPEN v_result FOR
      SELECT * FROM orders WHERE customer_id = p_customer_id;
    RETURN v_result;
  END get_customer_orders;

  -- 计算总销售额
  FUNCTION calculate_total_sales RETURN NUMBER IS
    v_total_sales NUMBER;
  BEGIN
    SELECT SUM(total_amount) INTO v_total_sales FROM orders;
    RETURN v_total_sales;
  END calculate_total_sales;

  -- 获取畅销商品列表
  FUNCTION get_top_selling_products(p_limit IN NUMBER) RETURN SYS_REFCURSOR IS
    v_result SYS_REFCURSOR;
  BEGIN
    OPEN v_result FOR
      SELECT product_id, product_name, SUM(quantity) AS total_quantity
      FROM orders JOIN products USING (product_id)
      GROUP BY product_id, product_name
      ORDER BY total_quantity DESC
      FETCH FIRST p_limit ROWS ONLY;
    RETURN v_result;
  END get_top_selling_products;

END sales_pkg;
/


```
![](img6.png)


- 调用函数、存储过程和查询：
 - 调用 create_order 过程创建订单：
```sql
BEGIN
  sales_pkg.create_order(p_customer_id => 1, p_product_id => 1, p_quantity => 5);
END;
/

```

 - 调用 get_customer_orders 函数获取特定客户的所有订单信息：
```sql
DECLARE
  v_orders_cursor SYS_REFCURSOR;
BEGIN
  v_orders_cursor := sales_pkg.get_customer_orders(p_customer_id => 1);

  -- 使用游标结果集进行处理
  -- 例如，可以使用 FETCH 和 LOOP 语句来遍历结果集并处理每个订单信息
  -- 这里只是简单地打印订单信息示例
  LOOP
    FETCH v_orders_cursor INTO /* 定义需要获取的订单信息的变量 */;
    EXIT WHEN v_orders_cursor%NOTFOUND;

    -- 处理订单信息
    /* 打印或处理订单信息的逻辑 */
  END LOOP;

  -- 关闭游标
  CLOSE v_orders_cursor;
END;
/

```

 - 调用 calculate_total_sales 函数计算总销售额：
```sql
DECLARE
  v_total_sales NUMBER;
BEGIN
  v_total_sales := sales_pkg.calculate_total_sales;
  DBMS_OUTPUT.PUT_LINE('Total Sales: ' || v_total_sales);
END;
/

```


 - 调用 get_top_selling_products 函数获取畅销商品列表：
```sql
DECLARE
  v_top_selling_cursor SYS_REFCURSOR;
BEGIN
  v_top_selling_cursor := sales_pkg.get_top_selling_products(p_limit => 10);

  -- 使用游标结果集进行处理
  -- 例如，可以使用 FETCH 和 LOOP 语句来遍历结果集并处理每个畅销商品信息
  -- 这里只是简单地打印商品信息示例
  LOOP
    FETCH v_top_selling_cursor INTO /* 定义需要获取的畅销商品信息的变量 */;
    EXIT WHEN v_top_selling_cursor%NOTFOUND;

    -- 处理畅销商品信息
    /* 打印或处理畅销商品信息的逻辑 */
  END LOOP;

  -- 关闭游标
  CLOSE v_top_selling_cursor;
END;
/

```

  
##### 第4步：设计一套数据库的备份方案。

- 此处使用rman进行oracle数据库的备份
- 分配通道使用指定格式备份数据库和归档日志文件，释放备份通道

- 使用sqlplus命令以sysdba用户身份登录到Oracle数据库，关闭数据库链接并以MOUNT模式重新启动数据库，将数据库切换到归档模式打开：
```sql
$ sqlplus / as sysdba
SHUTDOWN IMMEDIATE
STARTUP MOUNT
ALTER DATABASE ARCHIVELOG;
ALTER DATABASE OPEN;
```
![](img7.png)

- 使用连接到Oracle数据库展示rman配置信息：
```sql
$ rman target /
SHOW ALL;
```
![](img8.png)

- 对整个数据库执行备份：
```sql
RMAN> BACKUP DATABASE;
```
![](img9.png)

- 列出备份信息：
```sql
RMAN> LIST BACKUP;
```
![](img10.png)

