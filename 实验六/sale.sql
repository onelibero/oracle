-- 创建数据库用户
CREATE USER sales_user IDENTIFIED BY password;
GRANT CONNECT, RESOURCE TO sales_user;

-- 创建表空间
CREATE TABLESPACE sales_data
  DATAFILE '/home/oracle/Desktop/sales_data.dbf' SIZE 100M
  AUTOEXTEND ON NEXT 10M;

CREATE TABLESPACE sales_index
  DATAFILE '/home/oracle/Desktop/sales_index.dbf' SIZE 50M
  AUTOEXTEND ON NEXT 10M;

-- 创建商品表
CREATE TABLE products (
  product_id NUMBER PRIMARY KEY,
  product_name VARCHAR2(100),
  price NUMBER,
  quantity NUMBER,
  category_id NUMBER,
  CONSTRAINT fk_category_id FOREIGN KEY (category_id) REFERENCES categories(category_id)
);

-- 创建订单表
CREATE TABLE orders (
  order_id NUMBER PRIMARY KEY,
  customer_id NUMBER,
  order_date DATE,
  total_amount NUMBER,
  CONSTRAINT fk_customer_id FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);

-- 创建客户表
CREATE TABLE customers (
  customer_id NUMBER PRIMARY KEY,
  customer_name VARCHAR2(100),
  address VARCHAR2(200),
  phone VARCHAR2(20)
);

-- 创建分类表
CREATE TABLE categories (
  category_id NUMBER PRIMARY KEY,
  category_name VARCHAR2(100)
);

-- 创建索引
CREATE INDEX idx_product_name ON products(product_name);
CREATE INDEX idx_order_date ON orders(order_date);
CREATE INDEX idx_customer_name ON customers(customer_name);
CREATE INDEX idx_category_name ON categories(category_name);

-- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg AS
  PROCEDURE create_order(p_order_id NUMBER, p_customer_id NUMBER, p_order_date DATE);
  FUNCTION get_customer_orders(p_customer_id NUMBER) RETURN SYS_REFCURSOR;
  FUNCTION calculate_total_sales RETURN NUMBER;
  FUNCTION get_top_selling_products RETURN SYS_REFCURSOR;
END sales_pkg;
/

CREATE OR REPLACE PACKAGE BODY sales_pkg AS
  PROCEDURE create_order(p_order_id NUMBER, p_customer_id NUMBER, p_order_date DATE) IS
  BEGIN
    -- 实现创建订单的逻辑
  END create_order;
  
  FUNCTION get_customer_orders(p_customer_id NUMBER) RETURN SYS_REFCURSOR IS
    -- 实现获取特定客户的订单信息的逻辑
  END get_customer_orders;
  
  FUNCTION calculate_total_sales RETURN NUMBER IS
    -- 实现计算总销售额的逻辑
  END calculate_total_sales;
  
  FUNCTION get_top_selling_products RETURN SYS_REFCURSOR IS
    -- 实现获取畅销商品列表的逻辑
  END get_top_selling_products;
END sales_pkg;
/

-- 授权用户访问程序包
GRANT EXECUTE ON sales_pkg TO sales_user;
